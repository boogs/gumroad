const links = document.getElementsByTagName("a");
const validLinks = fetchLinks(links);
const iFrameCache = {};

window.onload = function() {
  document.addEventListener("click", buttonClick);
  document.addEventListener("mouseover", earlyLoad);
}

/*
 grab all links on page and filter valid URLs (custom subdomains and domains) (asynchronous)
 @return string[]
 */
function fetchLinks(links) {
  // support any type of custom domain
  //const urlSchema = ["([^.\s]+)([^?/]+)", "gumroad.com/l", "gum.co"];

  // I have only seen the below schemas before, but there could be more ^
  // support only the custom domains from problem description
  const urlSchema = ["sahil", "gumroad.com\/l\/", "gum.co\/"];

  // template literal
  //let regexFilter = new RegExp(`(https?:)?\/\/(www\.)?(${urlSchema})([^?/]+)`, 'gi');
  //const urlRegexSchemas = urlSchema.map(x => new RegExp(`^(?:http(s)?:\/\/)?(www\.)?(${x})([^?/]+)`, 'gi'));

  const urlRegexSchemas = urlSchema.map(x => new RegExp(`(https?:)?\/\/(www\.)?(${x})([^?/]+)`, "gi"));
  let filteredLinks = Array.prototype.filter.call(
    links,
    link => urlRegexSchemas.some(urlRegex => link.getAttribute("href").match(urlRegex))
  );

  return filteredLinks;
}

/*
 create popup on button click (asynchronous)
 @return void
 */
async function buttonClick(e) {
	// check with fetched links
  e.preventDefault();
  e.stopPropagation();
  const link = e.target.getAttribute("href");
  const iFrame = getIframe(link);
  const overlay = document.createElement("div");

  if (validLinks.includes(e.target)) {
    // load iFrame
    iFrame.setAttribute("src", link);
    iFrame.style.width = `${window.screen.width}px`;
    iFrame.style.height = `${window.screen.height}px`;
    iFrame.setAttribute("class", ".gumroad-iframe.visible");

    overlay.classList.add("gumroad-overlay-container");
    overlay.style.width = "100%";
    overlay.style.height = "100%";
    overlay.setAttribute("id", "clickbox");
    document.getElementsByTagName("body").item(0).appendChild(overlay);
    document.getElementsByTagName("body").item(0).appendChild(iFrame);
  } else {
    Array.prototype.slice.call(document.getElementsByTagName("iframe")).forEach(
      function(element) {
        //element.remove(); for newer browser
        element.parentNode.removeChild(element); //for older browsers e.g., Edge
      });
  }
}

/*
 do early load pages on hover (asynchronous)
 @return void
 */
async function earlyLoad(e) {
  // check with fetched links
  if (validLinks.includes(e.target)) {
    const link = e.target.getAttribute("href");
    const iFrame = getIframe(link);
    // load iFrame
    iFrame.setAttribute("src", link);
  }
}

/*
 check cache if iFrame exists ? return cached iFrame : create iFrame, cache, return new iFrame
 @return iframe
 */
function getIframe(url)  {
  const cachedIframe = iFrameCache[url];
  if (cachedIframe != null) { return cachedIframe; }

  const iFrame = document.createElement("iframe");
  iFrame.setAttribute("class", "gumroad-iframe");
  iFrame.setAttribute("srcrolling", "no");
  iFrame.setAttribute("allowFullScreen", "allowfullScreen");
  iFrame.setAttribute("id", "clickbox");
  iFrame.allowtransparency = !0;
  iFrameCache[url] = iFrame;

  return iFrame;
}
