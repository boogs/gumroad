#!/usr/bin/python3
from itertools import accumulate
from typing import List



# initial solution
# collects all pivots
def pivot_index_initial(arr: List[int]) -> int:
    """returns the pivot index of a list of integers"""
    if len(arr) < 3:
        return -1

    # collect all pivots in a list
    ans = []
    # accumulate the sum of all elements in the list and the corresponding pivot index
    sums = {value: (index + 1) for index, value in enumerate(list(accumulate(arr))[:-1])}
    # do the same as above but in reverse order
    reversed_sums = {value: index for index, value in enumerate(list(reversed(list(accumulate(arr[::-1]))[:-1])))}

    # As of Python 3.7, dictionaries are insertion ordered, so the below approach works but it may not for another language
    # iterate over all sums on the left side
    for k in sums:
        # check if the right side has an equal sum
        if k in reversed_sums:
            # check that the sum of the left and right side share the same pivot
            if sums[k] == reversed_sums[k]:
                # append pivot index to the list of all pivots
                ans.append(sums[k])

    # return left most pivot if pivot indices exist, otherwise return -1
    return ans[0] if len(ans) > 0 else -1


# concise solution
# does not collect all pivots like the approach above
def pivot_index(arr: List[int]) -> int:
    """returns the pivot index of a list of integers"""
    total = sum (arr)
    left_sum = 0

    for index, value in enumerate(arr):
        if left_sum == total - left_sum - value:
            return index
        left_sum += value

    return -1


if __name__ == "__main__":
    # create any array and pass it into the pivot_index function to test
    #arr = [1, 4, 6, 3, 2]
    arr = [1,8,9,8,1]
    print(pivot_index_initial(arr))
    print(pivot_index(arr))
